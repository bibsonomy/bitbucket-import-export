package org.bibsonomy.bitbucket;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringEscapeUtils;
import org.bibsonomy.util.HashUtils;
import org.bitbucket.IssueService;
import org.bitbucket.model.Attachment;
import org.bitbucket.model.Comment;
import org.bitbucket.model.Component;
import org.bitbucket.model.ImportExportIssueContainer;
import org.bitbucket.model.Issue;
import org.bitbucket.model.IssueKind;
import org.bitbucket.model.IssuePriority;
import org.bitbucket.model.IssueStatus;
import org.bitbucket.model.Log;
import org.bitbucket.model.Meta;
import org.bitbucket.model.Milestone;
import org.bitbucket.model.Version;

import au.com.bytecode.opencsv.CSVReader;

/**
 * @author dzo
 */
public class Converter {
	private static final String IMPORT_USER = "import_";
	
	/** encoding */
	protected static final String ENCODING = "UTF-8";
	
	private static final String COM_REST_CLIENT = "api";
	private static final String COM_SCRAPER = "scraper";
	private static final String COM_REST_SERVER = "rest-client";
	private static final String COM_DATABASE = "database";
	private static final String COM_WEBAPP = "webapp";

	public static void main(String[] args) throws Exception {
		final Map<String, String> userAliases = buildUserAliases(args[0]);
		int commentId = 1;
		final Map<Integer, Issue> issues = new HashMap<Integer, Issue>();
		final ImportExportIssueContainer container = generateBibSonomyContainer();
		final Map<String, Version> versionMap = new HashMap<String, Version>();
		final Map<String, Milestone> milestoneMap = new HashMap<String, Milestone>();
		
		/*
		 * first read issues
		 */
		final String basePath = args[1];
		final CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(basePath + "/issues.csv"), ENCODING));
		String[] line = null;

		while ((line = reader.readNext()) != null) {
			if (line.length != 10) {
				reader.close();
				System.err.println(line.length);
				System.exit(-1);
			}

			final int id = Integer.parseInt(line[0]);

			final Issue issue = new Issue();
			issue.setId(id);

			/*
			 * open date
			 */
			final long openDateInUnix = Long.parseLong(line[3]) * 1000;
			final Date openDate = new Date(openDateInUnix);
			issue.setCreatedOn(openDate);

			/*
			 * reporter
			 */
			final String submittedBy = extractName(userAliases, line[4]);
			issue.setReporter(submittedBy);

			/*
			 * assignee
			 */
			final String assignee = extractName(userAliases, line[5]);
			issue.setAssignee(assignee);

			/*
			 * close date
			 */
			final String closeDate = line[6];

			final long closeDateInUnix = Long.parseLong(closeDate) * 1000;
			if (closeDateInUnix != 0) {
				issue.setUpdatedOn(new Date(closeDateInUnix));
				issue.setStatus(IssueStatus.RESOLVED);
			} else {
				issue.setStatus(IssueStatus.NEW);
				updateIssueUpdateDate(issue, openDate);
			}

			/*
			 * title
			 */
			final String title = line[7];
			issue.setTitle(cleanTitle(title));
			
			/*
			 * description
			 */
			final String description = line[8];
			issue.setContent(cleanDescription(description));
			if (submittedBy.startsWith(IMPORT_USER)) {
				final String originalName = submittedBy.replaceFirst(IMPORT_USER, "");
				issue.setContent("Reported by " + originalName + ":\n" + issue.getContent());
			}
			/*
			 * prio
			 */
			final int priority = Integer.parseInt(line[2]);
			issue.setPriority(IssuePriority.values()[priority - 1]);

			/*
			 * set kind
			 */
			issue.setKind(IssueKind.valueOf(IssueKind.class, line[9]));

			issues.put(Integer.valueOf(id), issue);
			if (IssueStatus.RESOLVED != issue.getStatus()) {
				container.getIssues().add(issue);
			}
		}

		/*
		 * add comments
		 */
		final CSVReader readerComments = new CSVReader(new InputStreamReader(new FileInputStream(basePath + "/comments.csv"), ENCODING));
		String[] lineComments = null;

		while ((lineComments = readerComments.readNext()) != null) {
			if (lineComments.length != 4) {
				System.err.println(lineComments.length);
				System.exit(-1);
			}
			final Integer issueID = Integer.valueOf(Integer.parseInt(lineComments[0]));
			if (!issues.containsKey(issueID)) {
				System.err.println(issueID);
				System.exit(-1);
			}

			final Issue issue = issues.get(issueID);
			final Date createdOn = new Date(Long.parseLong(lineComments[2]) * 1000);
			updateIssueUpdateDate(issue, createdOn);
			
			final Comment comment = new Comment();
			comment.setId(commentId++);
			final String commenter = extractName(userAliases, lineComments[3]);
			comment.setUser(commenter);
			comment.setContent(cleanDescription(lineComments[1]));
			if (commenter.startsWith(IMPORT_USER)) {
				final String originalCommenter = commenter.replaceFirst(IMPORT_USER, "");
				comment.setContent("Commented by " + originalCommenter + ":\n" + comment.getContent());
			}
			comment.setIssueId(issueID);
			comment.setCreatedOn(createdOn);
			if (IssueStatus.RESOLVED != issue.getStatus()) {
				container.getComments().add(comment);
			}
		}
		readerComments.close();

		final CSVReader attachments = new CSVReader(new InputStreamReader(new FileInputStream(basePath + "/attachments.csv"), ENCODING));
		String[] lineAttachments = null;
		final String outputDir = basePath + "/output";
		final String attOutputDir = outputDir + "/attachments";
		new File(attOutputDir).mkdirs();

		while ((lineAttachments = attachments.readNext()) != null) {
			if (lineAttachments.length != 5) {
				System.err.println(lineAttachments.length);
				System.exit(-1);
			}

			final Integer issueID = Integer.valueOf(Integer.parseInt(lineAttachments[0]));
			if (!issues.containsKey(issueID)) {
				System.err.println(issueID);
				System.exit(-1);
			}
			final Issue issue = issues.get(issueID);
			if (IssueStatus.RESOLVED != issue.getStatus()) {
				final String data = lineAttachments[3];
				final String filename = lineAttachments[1];
				final String resultName = HashUtils.getMD5Hash(data.getBytes());
				
				final DataOutputStream stream = new DataOutputStream(new FileOutputStream(new File(attOutputDir + "/" + resultName)));
				stream.write(Base64.decodeBase64(data));
				stream.close();
				/*
				final Date attDate = new Date(Long.parseLong(lineAttachments[2]) * 1000);
				if (attDate.getTime() != issue.getCreatedOn().getTime()) {
					// TODO: create log
				}*/
				
				final Attachment attachment = new Attachment();
				attachment.setFilename(filename);
				attachment.setIssue(issueID);
				attachment.setPath("attachments/" + resultName);
				attachment.setUser(extractName(userAliases, lineAttachments[4]));
				container.getAttachments().add(attachment);
			}
		}
		attachments.close();
		
		final CSVReader extras = new CSVReader(new InputStreamReader(new FileInputStream(basePath + "/extras.csv"), ENCODING));
		String[] lineExtras = null;

		while ((lineExtras = extras.readNext()) != null) {
			if (lineExtras.length != 3) {
				System.err.println(lineExtras.length);
				System.exit(-1);
			}

			final Integer issueID = Integer.valueOf(Integer.parseInt(lineExtras[0]));
			if (!issues.containsKey(issueID)) {
				System.err.println(issueID);
				System.exit(-1);
			}
			final Issue issue = issues.get(issueID);
			if (IssueStatus.RESOLVED != issue.getStatus()) {
				final String key = lineExtras[1];
				final String value = lineExtras[2].trim();
				
				if ("version".equalsIgnoreCase(key)) {
					Version version = versionMap.get(value);
					if (!present(version)) {
						version = new Version();
						version.setName(value);
						container.getVersions().add(version);
					}
					
					issue.setVersion(version.getName());
				}
				
				if ("project".equals(key)) {
					Milestone milestone = milestoneMap.get(value);
					if (!present(milestone)) {
						milestone = new Milestone();
						milestone.setName(value);
						container.getMilestones().add(milestone);
					}
					
					issue.setMilestone(milestone.getName());
				}
			}
		}
		extras.close();
		
		final Writer w = new OutputStreamWriter(new FileOutputStream(outputDir + "/gforge.json"), ENCODING);
		new IssueService().serializeIssueImport(container, w);
		w.close();
		
		// TODO: add zipping
	}

	protected static void updateIssueUpdateDate(final Issue issue, final Date date) {
		final Date updatedOn = issue.getUpdatedOn();
		if (updatedOn == null) {
			issue.setUpdatedOn(date);
		} else if (updatedOn.before(date)) {
			issue.setUpdatedOn(date);
		}
	}
	
	protected static String cleanDescription(final String text) {
		return cleanTitle(text);
	}
	
	protected static String cleanTitle(final String title) {
		return StringEscapeUtils.unescapeHtml(title).replaceAll("\\r", "").replaceAll("\\^M", "").replaceAll("@see", "see");
	}

	/**
	 * @param userAliases
	 * @param string
	 * @return
	 */
	private static String extractName(Map<String, String> userAliases, String string) {
		if ("None".equalsIgnoreCase(string)) {
			return "";
		}

		if (userAliases.containsKey(string)) {
			return userAliases.get(string);
		}
		return IMPORT_USER + string;
	}

	protected static Map<String, String> buildUserAliases(final String file) throws IOException {
		final BufferedReader stream = new BufferedReader(new InputStreamReader(new FileInputStream(new File(file))));
		final Map<String, String> userMap = new HashMap<String, String>();

		String line = null;
		while ((line = stream.readLine()) != null) {
			final String[] useraliases = line.split(" ");
			userMap.put(useraliases[0].trim(), useraliases[1].trim());
		}
		stream.close();
		return userMap;
	}

	protected static ImportExportIssueContainer generateBibSonomyContainer() {
		final ImportExportIssueContainer container = new ImportExportIssueContainer();
		final List<Version> versions = new LinkedList<Version>();
		container.setVersions(versions);
		
		final List<Component> components = new LinkedList<Component>();
		components.add(new Component(COM_WEBAPP));
		components.add(new Component(COM_DATABASE));
		components.add(new Component(COM_REST_SERVER));
		components.add(new Component(COM_SCRAPER));
		components.add(new Component(COM_REST_CLIENT));
		container.setComponents(components);
		container.setAttachments(new LinkedList<Attachment>());
		final Meta meta = new Meta();
		meta.setDefaultKind("bug");
		container.setMeta(meta);
		container.setLogs(new LinkedList<Log>());
		container.setIssues(new LinkedList<Issue>());
		container.setComments(new LinkedList<Comment>());
		container.setMilestones(new LinkedList<Milestone>());
		return container;
	}
}
