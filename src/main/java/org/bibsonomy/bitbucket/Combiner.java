package org.bibsonomy.bitbucket;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

import org.bitbucket.IssueService;
import org.bitbucket.model.ImportExportIssueContainer;
import org.bitbucket.util.Utils;

/**
 * combines two bitbucket export formats
 *
 * @author dzo
 */
public class Combiner {
	
	/**
	 * 
	 * @param args	[0] path to first file
	 * 				[1] path to second file
	 * 				[2] path to result file
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		final IssueService issueService = new IssueService();
		final Reader firstReader = new InputStreamReader(new FileInputStream(args[0]), Converter.ENCODING);
		final ImportExportIssueContainer firstContainer = issueService.read(firstReader);
		firstReader.close();
		final Reader secondReader = new InputStreamReader(new FileInputStream(args[1]), Converter.ENCODING);
		final ImportExportIssueContainer secondContainer = issueService.read(secondReader);
		secondReader.close();
		final ImportExportIssueContainer mergedContainer = Utils.merge(firstContainer, secondContainer);
		final Writer writer = new OutputStreamWriter(new FileOutputStream(args[2]), Converter.ENCODING);
		issueService.serializeIssueImport(mergedContainer, writer);
		writer.close();
	}
}
