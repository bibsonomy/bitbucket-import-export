package org.bitbucket;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Date;

import org.bibsonomy.rest.renderer.impl.json.EnumSerializer;
import org.bitbucket.model.ImportExportIssueContainer;
import org.bitbucket.util.DateDeserializer;
import org.bitbucket.util.DateSerializer;
import org.bitbucket.util.DeserializerFactory;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.cfg.DeserializerFactoryConfig;
import com.fasterxml.jackson.databind.deser.DefaultDeserializationContext.Impl;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * 
 * @author dzo
 */
public class IssueService {
	
	/**
	 * serializes the object structure to the writer
	 * @param container
	 * @param writer
	 * @throws IOException
	 */
	public void serializeIssueImport(final ImportExportIssueContainer container, final Writer writer) throws IOException {
		final ObjectMapper mapper = createMapper();
		mapper.writeValue(writer, container);
	}
	
	/**
	 * reads a {@link ImportExportIssueContainer} from an input stream
	 * @param reader
	 * @return a parsed {@link ImportExportIssueContainer}
	 * @throws IOException
	 */
	public ImportExportIssueContainer read(final Reader reader) throws IOException {
		final ObjectMapper mapper = createMapper();
		return mapper.readValue(reader, ImportExportIssueContainer.class);
	}
	
	private static ObjectMapper createMapper() {
		final DeserializerFactory deserializeFactory = new DeserializerFactory(new DeserializerFactoryConfig());
		final ObjectMapper mapper = new ObjectMapper(null, null, new Impl(deserializeFactory));
		
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		final SimpleModule testModule = new SimpleModule("MyModule", new Version(1, 0, 0, null, null, null));
		testModule.addSerializer(new DateSerializer());
		testModule.addSerializer(new EnumSerializer());
		testModule.addDeserializer(Date.class, new DateDeserializer());
		mapper.registerModule(testModule);
		return mapper;
	}
}
