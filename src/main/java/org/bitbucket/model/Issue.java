package org.bitbucket.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dzo
 */
public class Issue implements Id {
	
	private int id;
	private String assignee;
	private String component;
	private String content;
	@JsonProperty("created_on")
	private Date createdOn;
	@JsonProperty("content_updated_on")
	private Date contentUpdatedOn;
	@JsonProperty("edited_on")
	private Date editedOn;
	private IssueKind kind;
	private String milestone;
	private IssuePriority priority;
	private String reporter;
	private IssueStatus status;
	private String title;
	@JsonProperty("updated_on")
	private Date updatedOn;
	private String version;
	private List<String> voters;
	private List<String> watchers;
	
	/**
	 * @return the id
	 */
	@Override
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	@Override
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the assignee
	 */
	public String getAssignee() {
		return assignee;
	}
	
	/**
	 * @param assignee the assignee to set
	 */
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	
	/**
	 * @return the component
	 */
	public String getComponent() {
		return component;
	}
	
	/**
	 * @param component the component to set
	 */
	public void setComponent(String component) {
		this.component = component;
	}
	
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the contentUpdatedOn
	 */
	public Date getContentUpdatedOn() {
		return contentUpdatedOn;
	}
	
	/**
	 * @param contentUpdatedOn the contentUpdatedOn to set
	 */
	public void setContentUpdatedOn(Date contentUpdatedOn) {
		this.contentUpdatedOn = contentUpdatedOn;
	}
	
	/**
	 * @return the editedOn
	 */
	public Date getEditedOn() {
		return editedOn;
	}
	
	/**
	 * @param editedOn the editedOn to set
	 */
	public void setEditedOn(Date editedOn) {
		this.editedOn = editedOn;
	}
	
	/**
	 * @return the kind
	 */
	public IssueKind getKind() {
		return kind;
	}
	
	/**
	 * @param kind the kind to set
	 */
	public void setKind(IssueKind kind) {
		this.kind = kind;
	}
	
	/**
	 * @return the milestone
	 */
	public String getMilestone() {
		return milestone;
	}
	
	/**
	 * @param milestone the milestone to set
	 */
	public void setMilestone(String milestone) {
		this.milestone = milestone;
	}
	
	/**
	 * @return the priority
	 */
	public IssuePriority getPriority() {
		return priority;
	}
	
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(IssuePriority priority) {
		this.priority = priority;
	}
	
	/**
	 * @return the reporter
	 */
	public String getReporter() {
		return reporter;
	}
	
	/**
	 * @param reporter the reporter to set
	 */
	public void setReporter(String reporter) {
		this.reporter = reporter;
	}
	
	/**
	 * @return the status
	 */
	public IssueStatus getStatus() {
		return status;
	}
	
	/**
	 * @param status the status to set
	 */
	public void setStatus(IssueStatus status) {
		this.status = status;
	}
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}
	
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	
	/**
	 * @return the voters
	 */
	public List<String> getVoters() {
		return voters;
	}
	
	/**
	 * @param voters the voters to set
	 */
	public void setVoters(List<String> voters) {
		this.voters = voters;
	}
	
	/**
	 * @return the watchers
	 */
	public List<String> getWatchers() {
		return watchers;
	}
	
	/**
	 * @param watchers the watchers to set
	 */
	public void setWatchers(List<String> watchers) {
		this.watchers = watchers;
	}
}
