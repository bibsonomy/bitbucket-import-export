package org.bitbucket.model;

/**
 * 
 * @author dzo
 */
public enum IssueKind {
	/**
	 * bug
	 */
	BUG,
	/**
	 * enhancement
	 */
	ENHANCEMENT,
	/**
	 * proposal
	 */
	PROPOSAL,
	/**
	 * task
	 */
	TASK
}
