package org.bitbucket.model;

import static org.bibsonomy.util.ValidationUtils.present;

/**
 * 
 * @author dzo
 */
public enum IssueStatus {
	/**
	 * issue is new
	 */
	NEW,
	/**
	 * issue verified on work
	 */
	OPEN,
	/**
	 * issue is resolved
	 */
	RESOLVED,
	/**
	 * issue is blocked by something
	 */
	ON_HOLD("on hold"),
	/**
	 * issue can't be reproduced
	 */
	INVALID,
	/**
	 * issue is a duplicate of another issue
	 */
	DUPLICATE;
	
	private String string;
	private IssueStatus() {
		this.string = null;
	}
	
	private IssueStatus(final String string) {
		this.string = string;
	}
	
	@Override
	public String toString() {
		if (present(string)) {
			return string;
		}
		return super.toString();
	}
}
