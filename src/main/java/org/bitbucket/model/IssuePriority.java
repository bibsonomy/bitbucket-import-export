package org.bitbucket.model;

/**
 * 
 * @author dzo
 */
public enum IssuePriority {
	/**
	 * doesn't take so long
	 */
	TRIVIAL,
	/** TODO */
	MINOR,
	/** TODO */
	MAJOR,
	/** TODO */
	CRITICAL,
	/** TODO */
	BLOCKER
}
