package org.bitbucket.model;

import java.util.List;

/**
 * 
 * @author dzo
 */
public class ImportExportIssueContainer {
	private Meta meta;
	private List<Attachment> attachments;
	private List<Version> versions;
	private List<Component> components;
	private List<Issue> issues;
	private List<Comment> comments;
	private List<Log> logs;
	private List<Milestone> milestones;
	
	/**
	 * @return the meta
	 */
	public Meta getMeta() {
		return meta;
	}

	/**
	 * @param meta the meta to set
	 */
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	/**
	 * @return the attachments
	 */
	public List<Attachment> getAttachments() {
		return attachments;
	}
	
	/**
	 * @param attachments the attachments to set
	 */
	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}
	
	/**
	 * @return the versions
	 */
	public List<Version> getVersions() {
		return versions;
	}
	
	/**
	 * @param versions the versions to set
	 */
	public void setVersions(List<Version> versions) {
		this.versions = versions;
	}
	
	/**
	 * @return the components
	 */
	public List<Component> getComponents() {
		return components;
	}
	
	/**
	 * @param components the components to set
	 */
	public void setComponents(List<Component> components) {
		this.components = components;
	}
	
	/**
	 * @return the issues
	 */
	public List<Issue> getIssues() {
		return issues;
	}
	
	/**
	 * @param issues the issues to set
	 */
	public void setIssues(List<Issue> issues) {
		this.issues = issues;
	}
	
	/**
	 * @return the comments
	 */
	public List<Comment> getComments() {
		return comments;
	}
	
	/**
	 * @param comments the comments to set
	 */
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	/**
	 * @return the logs
	 */
	public List<Log> getLogs() {
		return logs;
	}
	
	/**
	 * @param logs the logs to set
	 */
	public void setLogs(List<Log> logs) {
		this.logs = logs;
	}

	/**
	 * @return the milestones
	 */
	public List<Milestone> getMilestones() {
		return milestones;
	}

	/**
	 * @param milestones the milestones to set
	 */
	public void setMilestones(List<Milestone> milestones) {
		this.milestones = milestones;
	}
}
