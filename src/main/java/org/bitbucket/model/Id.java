package org.bitbucket.model;

/**
 * common interface for items that have an id
 *
 * @author dzo
 */
public interface Id {
	
	/**
	 * set the id
	 * @param id
	 */
	public void setId(int id);

	/**
	 * @return the id
	 */
	public int getId();

}
