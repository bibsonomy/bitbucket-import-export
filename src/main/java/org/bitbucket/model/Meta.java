package org.bitbucket.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dzo
 */
public class Meta {
	@JsonProperty("default_assignee")
	private String defaultAssignee;
	
	@JsonProperty("default_component")
	private String defaultComponent;
	
	@JsonProperty("default_kind")
	private String defaultKind;
	
	@JsonProperty("default_milestone")
	private String defaultMilestone;
	
	@JsonProperty("default_version")
	private String defaultVersion;

	/**
	 * @return the defaultAssignee
	 */
	public String getDefaultAssignee() {
		return defaultAssignee;
	}

	/**
	 * @param defaultAssignee the defaultAssignee to set
	 */
	public void setDefaultAssignee(String defaultAssignee) {
		this.defaultAssignee = defaultAssignee;
	}

	/**
	 * @return the defaultComponent
	 */
	public String getDefaultComponent() {
		return defaultComponent;
	}

	/**
	 * @param defaultComponent the defaultComponent to set
	 */
	public void setDefaultComponent(String defaultComponent) {
		this.defaultComponent = defaultComponent;
	}

	/**
	 * @return the defaultKind
	 */
	public String getDefaultKind() {
		return defaultKind;
	}

	/**
	 * @param defaultKind the defaultKind to set
	 */
	public void setDefaultKind(String defaultKind) {
		this.defaultKind = defaultKind;
	}

	/**
	 * @return the defaultMilestone
	 */
	public String getDefaultMilestone() {
		return defaultMilestone;
	}

	/**
	 * @param defaultMilestone the defaultMilestone to set
	 */
	public void setDefaultMilestone(String defaultMilestone) {
		this.defaultMilestone = defaultMilestone;
	}

	/**
	 * @return the defaultVersion
	 */
	public String getDefaultVersion() {
		return defaultVersion;
	}

	/**
	 * @param defaultVersion the defaultVersion to set
	 */
	public void setDefaultVersion(String defaultVersion) {
		this.defaultVersion = defaultVersion;
	}
}
