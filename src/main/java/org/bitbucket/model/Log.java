package org.bitbucket.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dzo
 */
public class Log {
	@JsonProperty("changed_from")
	private String changedFrom;
	
	@JsonProperty("changed_to")
	private String changedTo;
	
	@JsonProperty("comment")
	private int commentId;
	
	@JsonProperty("created_on")
	private Date createdOn;
	
	private String field;
	
	@JsonProperty("issue")
	private int issueId;
	
	private String user;

	/**
	 * @return the changedFrom
	 */
	public String getChangedFrom() {
		return changedFrom;
	}

	/**
	 * @param changedFrom the changedFrom to set
	 */
	public void setChangedFrom(String changedFrom) {
		this.changedFrom = changedFrom;
	}

	/**
	 * @return the changedTo
	 */
	public String getChangedTo() {
		return changedTo;
	}

	/**
	 * @param changedTo the changedTo to set
	 */
	public void setChangedTo(String changedTo) {
		this.changedTo = changedTo;
	}

	/**
	 * @return the commentId
	 */
	public int getCommentId() {
		return commentId;
	}

	/**
	 * @param commentId the commentId to set
	 */
	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}

	/**
	 * @param field the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}

	/**
	 * @return the issueId
	 */
	public int getIssueId() {
		return this.issueId;
	}

	/**
	 * @param issueId the issueId to set
	 */
	public void setIssueId(int issueId) {
		this.issueId = issueId;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
}
