package org.bitbucket.util;

import java.io.IOException;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;

/**
 * date deserializer that uses the {@link DateSerializer#FORMATTER}
 * 
 * @author dzo
 */
public class DateDeserializer extends StdScalarDeserializer<Date> {
	private static final long serialVersionUID = -8222038500681010016L;

	public DateDeserializer() {
		super(Date.class);
	}

	@Override
	public Date deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {

		if (jp.getCurrentToken() == JsonToken.VALUE_STRING) {
			String str = jp.getText().trim();
			if (str.length() == 0) {
				return (Date) getEmptyValue();
			}
			
			return DateSerializer.FORMATTER.parseDateTime(str).toDate();
		}
		return null;
	}

}
