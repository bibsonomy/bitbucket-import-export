package org.bitbucket.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.bitbucket.model.Comment;
import org.bitbucket.model.Component;
import org.bitbucket.model.Id;
import org.bitbucket.model.ImportExportIssueContainer;
import org.bitbucket.model.Issue;
import org.bitbucket.model.Log;
import org.bitbucket.model.Milestone;
import org.bitbucket.model.Version;

/**
 * some helper methods to interact with the model
 *
 * @author dzo
 */
public final class Utils {
	private Utils() {}

	/**
	 * @param container
	 * @return all issue ids
	 */
	private static SortedSet<Integer> extractIssueIds(ImportExportIssueContainer container) {
		return extractIds(container.getIssues());
	}

	/**
	 * @param firstContainer
	 * @param secondContainer
	 * @return the changed issue ids of the secondContainer
	 */
	private static Map<Integer, Integer> mergeIssues(ImportExportIssueContainer firstContainer, ImportExportIssueContainer secondContainer) {
		final Map<Integer, Integer> changedIds = new HashMap<Integer, Integer>();
		final SortedSet<Integer> issueIds = extractIssueIds(firstContainer);
		for (final Issue issue : secondContainer.getIssues()) {
			final int oldIssueId =  issue.getId();
			if (issueIds.contains(Integer.valueOf(oldIssueId))) {
				final int newIssueId = issueIds.last().intValue() + 1;
				issue.setId(newIssueId);
			}
			
			final int newIssueID = issue.getId();
			firstContainer.getIssues().add(issue);
			issueIds.add(Integer.valueOf(newIssueID));
			changedIds.put(Integer.valueOf(oldIssueId), Integer.valueOf(newIssueID));
		}
		
		return changedIds;
	}

	/**
	 * @param firstContainer
	 * @param secondContainer
	 */
	private static void mergeVersions(ImportExportIssueContainer firstContainer, ImportExportIssueContainer secondContainer) {
		final List<Version> firstVersions = firstContainer.getVersions();
		for (final Version version : secondContainer.getVersions()) {
			if (!firstVersions.contains(version)) {
				firstVersions.add(version);
			}
		}
	}

	/**
	 * @param firstContainer
	 * @param secondContainer
	 */
	private static void mergeMigrations(ImportExportIssueContainer firstContainer, ImportExportIssueContainer secondContainer) {
		final List<Milestone> firstMilestones = firstContainer.getMilestones();
		for (final Milestone milestone : secondContainer.getMilestones()) {
			if (!firstMilestones.contains(milestone)) {
				firstMilestones.add(milestone);
			}
		}
	}

	/**
	 * @param firstContainer
	 * @param secondContainer
	 * @param changedIssueIds 
	 */
	private static void mergeComments(ImportExportIssueContainer firstContainer, ImportExportIssueContainer secondContainer, Map<Integer, Integer> changedIssueIds) {
		final SortedSet<Integer> commentIds = extractCommentIds(firstContainer);
		for (final Comment comment : secondContainer.getComments()) {
			// fix duplicate comment id
			if (commentIds.contains(Integer.valueOf(comment.getId()))) {
				final int newCommentId = commentIds.last().intValue() + 1;
				comment.setId(newCommentId);
			}
			
			Integer issueId = Integer.valueOf(comment.getIssueId());
			if (changedIssueIds.containsKey(issueId)) {
				comment.setIssueId(changedIssueIds.get(issueId).intValue());
			}
			
			firstContainer.getComments().add(comment);
			commentIds.add(Integer.valueOf(comment.getId()));
		}
	}

	/**
	 * @param container
	 * @return all comment ids
	 */
	private static SortedSet<Integer> extractCommentIds(ImportExportIssueContainer container) {
		return extractIds(container.getComments());
	}
	
	private static SortedSet<Integer> extractIds(Collection<? extends Id> collection) {
		final SortedSet<Integer> ids = new TreeSet<Integer>();
		for (final Id id : collection) {
			ids.add(Integer.valueOf(id.getId()));
		}
		return ids;
	}

	/**
	 * @param firstContainer
	 * @param secondContainer
	 */
	private static void mergeComponents(ImportExportIssueContainer firstContainer, ImportExportIssueContainer secondContainer) {
		final List<Component> firstComponents = firstContainer.getComponents();
		for (Component component : secondContainer.getComponents()) {
			if (!firstComponents.contains(component)) {
				firstComponents.add(component);
			}
		}
	}

	/**
	 * @param firstContainer
	 * @param secondContainer
	 */
	private static void mergeAttachments(ImportExportIssueContainer firstContainer, ImportExportIssueContainer secondContainer) {
		// TODO same path
		firstContainer.getAttachments().addAll(secondContainer.getAttachments());
	}

	/**
	 * @param firstContainer
	 * @param secondContainer
	 * @param changedIssueIds 
	 */
	private static void mergeLogs(ImportExportIssueContainer firstContainer, ImportExportIssueContainer secondContainer, Map<Integer, Integer> changedIssueIds) {
		final SortedSet<Integer> commentIds = extractCommentIds(firstContainer);
		for (final Log log : secondContainer.getLogs()) {
			// fix duplicate comment id
			if (commentIds.contains(Integer.valueOf(log.getCommentId()))) {
				final int newId = commentIds.last().intValue() + 1;
				log.setCommentId(newId);
			}
			
			final Integer issueId = Integer.valueOf(log.getIssueId());
			if (changedIssueIds.containsKey(issueId)) {
				log.setIssueId(changedIssueIds.get(issueId).intValue());
			}
			
			firstContainer.getLogs().add(log);
			commentIds.add(Integer.valueOf(log.getCommentId()));
		}
	}

	/**
	 * @param firstContainer
	 * @param secondContainer
	 * @return the merged container (based on firstContainer)
	 */
	public static ImportExportIssueContainer merge(ImportExportIssueContainer firstContainer, ImportExportIssueContainer secondContainer) {
		mergeVersions(firstContainer, secondContainer);
		mergeMigrations(firstContainer, secondContainer);
		mergeComponents(firstContainer, secondContainer);
		final Map<Integer, Integer> changedIssueIds = mergeIssues(firstContainer, secondContainer);
		mergeComments(firstContainer, secondContainer, changedIssueIds);
		
		// mergeLogs must be called after mergeComments
		mergeLogs(firstContainer, secondContainer, changedIssueIds);
		mergeAttachments(firstContainer, secondContainer);
		
		return firstContainer;
	}
}
