/*
 * replace 480 with the id of the tracker
 */

\copy ( SELECT a.artifact_id, a.status_id, a.priority, a.open_date, u1.user_name, u2.user_name,  a.close_date, a.summary, a.details, 'BUG' as kind FROM artifact a LEFT JOIN users u1 ON (a.submitted_by = u1.user_id) LEFT JOIN users u2 ON (a.assigned_to = u2.user_id) WHERE a.group_artifact_id = 480 ) TO '/tmp/480_issues.csv' DELIMITER ',' CSV
\copy ( SELECT am.artifact_id, am.body, am.adddate, u1.user_name FROM artifact_message am LEFT JOIN users u1 ON(am.submitted_by = u1.user_id) JOIN artifact a USING(artifact_id) WHERE a.group_artifact_id = 480 ) TO '/tmp/480_comments.csv' DELIMITER ',' CSV
\copy ( SELECT af.artifact_id, af.filename, af.adddate, af.bin_data, u1.user_name FROM artifact_file af LEFT JOIN users u1 ON(af.submitted_by = u1.user_id) JOIN artifact a USING(artifact_id) WHERE a.group_artifact_id = 480 ) TO '/tmp/480_attachments.csv' DELIMITER ',' CSV
\copy ( select a.artifact_id, field_name, element_name from artifact_extra_field_data join artifact_extra_field_elements USING(extra_field_id) join artifact_extra_field_list USING(extra_field_id) join artifact a USING(artifact_id) where field_data::int = element_id AND a.group_artifact_id = 480 ) TO '/tmp/480_extras.csv' DELIMITER ',' CSV